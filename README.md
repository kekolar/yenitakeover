bu programı kullanabilmek için azure cli ve aws cli programlarının kurulmuş olması gerekmektedir. Aynı zamanda azure cli uygulaması ile terminalden login olmanız gerekmektedir. AWS cli ile de aws hesabınızdan bütün izinlerin açık olduğu bir accesskey ve secretkey ile configurasyon yapmanız gerekmektedir. aws cli ile elasticbeanstalk sorgusu yapabildiğinizi manuel test ettikten sonra ve configurasyonların doğrulamasını onaylayınız. Daha sonra telagramdan bir grup oluşturunuz. oluşturuğunuz grubun telegram chatid ve telegram key'i takeover.py içindeki "self.telegram_chatid" ve "self.telegram_key" değişkenlerine yazınız. Böyle takeover sonuçlarını telegram kanalınıza gönderebileceksiniz. Ayrıca Fastly hesabı açın ve fastly service id bilgisini ve fasty keyiniz'i takeover.py içindeki "self.fastly_service_id" ve "self.fastly_key" değişkenlerine yazınız.

install:

```
pip3 install -r requirements.txt
```


```
cat all_subdomains.txt | python3 takeover.py --stdin --thread 100 --output results.txt
```
