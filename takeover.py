#Author: CyberTheReaper
#Hackerone: https://hackerone.com/isimsiz
#Bugcrowd:  https://bugcrowd.com/hacktivist1337

import re
import os
import sys
import requests
import threading
import argparse
import threading
import dns.resolver
import tldextract
import warnings
from concurrent.futures import ThreadPoolExecutor

requests.packages.urllib3.disable_warnings()
warnings.filterwarnings("ignore")

class attack():

    def __init__(self):

        self.telegram_key = "" #telegram channel key
        self.telegram_chatid = "" #telegram chatid
        self.fastly_service_id = "0FaRjA6qtq4EAlWd3KnZMO" #fastly service_id
        self.fastly_key = "POAwxaEHEQdg_TeoZMAmmqmW7TryHV8Z" #fastly auth key
        self.fastly_total = []
        self.print_lock = threading.Lock()
        self.azure_regex = re.compile("(.*\.trafficmanager\.net$|.*\.cloudapp\.net$|.*\.azure\.com$|.*\.azurewebsites\.net$|.*\.windows\.net$|.*\.azure-api\.net$|.*\.azurehdinsight\.net$|.*\.azureedge\.net$|.*\.azurecontainer\.io$|.*\.azuredatalakestore\.net$|.*\.azurecr\.io$|.*\.visualstudio\.com$|.*\.elasticbeanstalk\.com$)")
        self.header = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36"}

        if args.stdin:
            self.targets = self.genfunc(list(set(filter(None, sys.stdin.read().split("\n")))))

        else:
            print("Yanlıs parametre girdiniz")
            sys.exit()


        self.Dnspython_Resolver = dns.resolver.Resolver()
        self.Dnspython_Resolver.timeout = 7.0
        self.Dnspython_Resolver.lifetime = 7.0
        self.Dnspython_Resolver.nameservers = ['1.1.1.1','1.0.0.1','8.8.8.8','8.8.4.4','77.88.8.8','77.88.8.1']



        with ThreadPoolExecutor(max_workers=args.thread) as executor:
            executor.map(self.dns_tester, self.targets)

    def genfunc(self, target):
        yield from target

    def dns_tester(self, target):

        try:

            if not ".-" in target and not "-." in target and not "._" in target and not "_." in target:
                query = self.Dnspython_Resolver.resolve(str(target), "A")

                if not "127.0.0.1" in str(query.response.answer) and not "0.0.0.0" in str(query.response.answer):
                    self.request_sender(target)


        except dns.resolver.NXDOMAIN as nx:
            cnames = []
            for msg in nx.kwargs["responses"].values():
                cnames += [cname[0].target for cname in msg.resolve_chaining().cnames]

            if cnames:
                nx_cname = str(cnames[0])
                if nx_cname.endswith("."):
                    nx_cname = nx_cname[:-1]

                match_cname = self.azure_regex.match(nx_cname)

                if match_cname != None:
                    if match_cname.string.endswith(".trafficmanager.net"):
                        x = os.popen(f"az network traffic-manager profile check-dns -n {match_cname.string[:-19]}").read().split("\n")
                        if '  "nameAvailable": true,' in x:

                            with self.print_lock:
                                print(f"[%100 VULNERABLE-TRAFFICMANAGER] {target} ==> {match_cname.string.lower()}")

                            if self.telegram_key and self.telegram_chatid:
                                self.telegram_sender(f"[VULNERABLE-TRAFFICMANAGER(VERIFED)]\nSub: {target}\nVuln: {match_cname.string.lower()}")

                            if args.output:
                                self.save_output(f"[AZURE-TRAFFICMANAGER] {str(target)}")

                    elif match_cname.string.endswith(".azureedge.net"):
                        x = os.popen(f"az cdn name-exists -n {match_cname.string.split('.')[-3]}").read().split("\n")
                        if '  "nameAvailable": true,' in x:

                            with self.print_lock:
                                print(f"[%100 VULNERABLE-AZUREEDGE(VERIFED)] {target} ==> {match_cname.string.lower()}")

                            if self.telegram_key and self.telegram_chatid:
                                self.telegram_sender(f"[VULNERABLE-AZUREEDGE(VERIFED)]\nSub: {target}\nVuln: {match_cname.string.lower()}")

                            if args.output:
                                self.save_output(f"[AZURE-AZUREEDGE] {str(target)}")

                    elif match_cname.string.endswith(".azure-api.net"):
                        x = os.popen(f"az apim check-name -n {match_cname.string.split('.')[-3]} 2>&1").read().split("\n")
                        if '  "nameAvailable": true,' in x:

                            with self.print_lock:
                                print(f"[%100 VULNERABLE-AZUREAPI(VERIFED)] {target} ==> {match_cname.string.lower()}")

                            if self.telegram_key and self.telegram_chatid:
                                self.telegram_sender(f"[VULNERABLE-AZUREAPI(VERIFED)]\nSub: {target}\nVuln: {match_cname.string.lower()}")

                            if args.output:
                                self.save_output(f"[AZURE-AZUREAPI] {str(target)}")

                    elif match_cname.string.endswith(".blob.core.windows.net"):
                        x = os.popen(f"az storage account check-name -n {match_cname.string.split('.')[-5]}").read().split("\n")
                        if '  "nameAvailable": true,' in x:

                            with self.print_lock:
                                print(f"[%100 VULNERABLE-AZUREBLOB(VERIFED)] {target} ==> {match_cname.string.lower()}")

                            if self.telegram_key and self.telegram_chatid:
                                self.telegram_sender(f"[VULNERABLE-AZUREBLOB(VERIFED)]\nSub: {target}\nVuln: {match_cname.string.lower()}")

                            if args.output:
                                self.save_output(f"[AZURE-BLOB] {str(target)}")

                    elif match_cname.string.endswith(".elasticbeanstalk.com"):
                        r = re.search(r"\.(us(-gov)?|ap|ca|cn|eu|sa)-(central|(north|south)?(east|west)?)-\d\.elasticbeanstalk\.com", match_cname.string)
                        if r:
                            x = os.popen(f"aws elasticbeanstalk check-dns-availability --region {match_cname.string.split('.')[-3]} --cname-prefix {match_cname.string.split('.')[-4]}").read().split("\n")
                            if '    "Available": true,' in x:

                                with self.print_lock:
                                    print(f"[%100 VULNERABLE-AWS-ELASTICBEANSTALK(VERIFED)] {target} ==> {match_cname.string.lower()}")

                                if self.telegram_key and self.telegram_chatid:
                                    self.telegram_sender(f"[VULNERABLE-AWS-ELASTICBEANSTALK(VERIFED)]\nSub: {target}\nVuln: {match_cname.string.lower()}")

                                if args.output:
                                    self.save_output(f"[AWS-ELASTICBEANSTALK] {str(target)}")
                    else:
                        with self.print_lock:
                            print(f"[%100 VULNERABLE-UNKNOWN-AZURE-SERVICE)] {target} ==> {match_cname.string.lower()}")

                        if self.telegram_key and self.telegram_chatid:
                            self.telegram_sender(f"[VULNERABLE-UNKNOWN-AZURE-SERVICE]\nSub: {target}\nVuln: {match_cname.string.lower()}")

                        if args.output:
                            self.save_output(f"[UNKNOWN-AZURE-SERVICE] {str(target)}")

                else:
                    try:

                        parcala = tldextract.extract(nx_cname).registered_domain

                        r = requests.post("https://www.atakdomain.com/service/domain/single", json={"domain":parcala}, headers=self.header, verify=False, timeout=10)

                        if "Sepete Ekle" in r.text:

                            with self.print_lock:
                                print(f"[%100 VULNERABLE-DOMAIN-AVAILABLE(VERIFED)] {target} ==> {nx_cname}")

                            if self.telegram_key and self.telegram_chatid:
                                self.telegram_sender(f"[VULNERABLE-DOMAIN-AVAILABLE(VERIFED)]\nSub: {target}\nVuln: {nx_cname}")

                            if args.output:
                                self.save_output(f"[DOMAIN-AVAILABLE] {str(target)}")
                    except:
                        pass
            else:
                pass
        except:
            pass

    def request_sender(self,subdomain):

        try:

            website = "http://" + str(subdomain)
            response = requests.get(website, verify=False, headers=self.header, timeout=10).text
            self.takeover_checker(response, subdomain)

        except requests.exceptions.ConnectionError:
            website = "https://" + str(subdomain)
            response = requests.get(website, verify=False, headers=self.header, timeout=10).text
            self.takeover_checker(response, subdomain)

        except:
            pass

    def takeover_checker(self, req, sub):

        #S3 BUCKET
        if "<Code>NoSuchBucket</Code>" in req and "<BucketName>" in req:
            bucket_name = re.findall("<BucketName>.*</BucketName>",req)[0].replace("<BucketName>","").replace("</BucketName>","") + ".s3.amazonaws.com"

            with self.print_lock:
                print(f"[%100 VULNERABLE-S3BUCKET(VERIFED)] {sub} ==> {bucket_name}")

            if self.telegram_key and self.telegram_chatid:
                self.telegram_sender(f'[VULNERABLE-S3BUCKET(VERIFED)]\nSub: {sub}\nVuln: {bucket_name}')

            if args.output:
                self.save_output(f"[AWS-S3BUCKET] {str(sub)}")

        #fastly
        elif "Fastly error: unknown domain:" in req:

            try:
                sub_domain = tldextract.extract(str(sub)).registered_domain
                if not sub_domain in self.fastly_total:
                    self.fastly_total.append(sub_domain)

                    if self.fastly_service_id and self.fastly_key:

                        try:

                            r = requests.post("https://api.fastly.com/service/" + self.fastly_service_id + "/version/1/domain", verify=False, json={"name":sub}, allow_redirects=True, timeout=10, headers={"User-Agent": "Mozilla Firefox 55/66", "Content-Type": "application/json; charset=UTF-8", "Fastly-Key": self.fastly_key})

                            if r.status_code == 422:
                                with self.print_lock:
                                    print(f"[%100 VULNERABLE-FASTLY(VERIFED)] {sub}")

                                if self.telegram_key and self.telegram_chatid:
                                    self.telegram_sender(f'[VULNERABLE-FASTLY(VERIFED)]\nSub: {sub}')

                                if args.output:
                                    self.save_output(f"[FASTLY] {str(sub)}")
                        except:
                            pass

            except:
                pass


    def telegram_sender(self,message):

        try:

            r = requests.get(f"https://api.telegram.org/bot{self.telegram_key}/sendMessage?chat_id={self.telegram_chatid}&text={message}", verify=False, headers=self.header, timeout=10)

        except:

            pass

    def save_output(self,target):

        with open(args.output, "a+", encoding="utf-8", errors="ignore") as file:
            file.write(str(target) + "\n")

if __name__ == "__main__":

    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--stdin", required=True, action="store_true", help="Read Subdomains From Stdin")
    ap.add_argument("-o", "--output", required=False, metavar="", help="Save output")
    ap.add_argument("-t", "--thread", required=False, default=50, type=int, metavar="",help="Thread Number(Default-50)")
    args = ap.parse_args()

    start_attack = attack()
